﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BankRecon
{
    public class Leitor
    {

        public static void Orquestrador(string CNABPath)
        {
            var CNABFile = File.ReadAllLines(CNABPath).ToList();


            switch (CNABFile[0].Substring(0,3))
            {
                case "422":
                    {
                        Leitor.SafraHeaderArquivo(CNABFile[0]);


                        //CNABFile.RemoveRange(0, 1);

                        var teste = CNABFile.Skip(1).Select(x => x.ToString()).ToList();

                        foreach (string line in CNABFile.Skip(1))
                        {

                            if (line == CNABFile.Last())
                            {
                                Leitor.ItauTrailerArquivo(line);
                            }


                            switch (line.Substring(0,3))
                            {
                                case "":
                                    {

                                    }
                                    break;


                                default:
                                    break;
                            }


                            Leitor.ItauHeaderLoteCCorrente(line);

                            Leitor.ItauDetalheECCorrente(line);
                        }


                    }
                    break;

                case "033":
                    {
                        //Leitor.saf
                    }

                    break;



                default:
                    {

                    }

                    break;
            }


        }

        public static void ItauHeaderArquivo(List<string> CNABFile)
        {
            

            foreach (var line in CNABFile)
            {
                string IdBanco = line.Substring(0, 3);
                string CodigoLote = line.Substring(3, 4);
                string TpRegistro = line.Substring(7, 1);
                string Filler1 = line.Substring(8, 9);
                string TpInscricao = line.Substring(17, 1);
                string NrInscricao = line.Substring(18, 14);
                string Filler2 = line.Substring(32, 15);
                string Convenio = line.Substring(47, 5);
                string Filler3 = line.Substring(52, 1);
                string NrAgencia = line.Substring(53, 4);
                string DigitoAgencia = line.Substring(57, 1);
                string Filler4 = line.Substring(58, 7);
                string CCEmpresa = line.Substring(65, 5);
                string Filler5 = line.Substring(70, 1);
                string DigitoAgenciaConta = line.Substring(71, 1);
                string NomeEmpresa = line.Substring(72, 30);
                string NomeBanco = line.Substring(102, 30);
                string Filler6 = line.Substring(132, 10);
                string CodigoRetorno = line.Substring(142, 1);
                string DataGeracao = line.Substring(143, 8);
                string HoraGeracao = line.Substring(151, 6);
                string NrArquivo = line.Substring(157, 6);
                string VersaoLayout = line.Substring(163, 3);
                string Filler7 = line.Substring(166, 5);
                string ExclusivoBanco = line.Substring(171, 20);
                string Filler9 = line.Substring(191, 49);

            }
        }

        public static void SafraHeaderArquivo(string line)
        {

            string IdBanco = line.Substring(0, 3);
            string CodigoLote = line.Substring(3, 4);
            string TpRegistro = line.Substring(7, 1);
            string Filler1 = line.Substring(8, 9);
            string TpInscricao = line.Substring(17, 1);
            string NrInscricao = line.Substring(18, 14);
            string Filler2 = line.Substring(32, 15);
            string Convenio = line.Substring(47, 5);
            string Filler3 = line.Substring(52, 1);
            string NrAgencia = line.Substring(53, 4);
            string DigitoAgencia = line.Substring(57, 1);
            string Filler4 = line.Substring(58, 7);
            string CCEmpresa = line.Substring(65, 5);
            string Filler5 = line.Substring(70, 1);
            string DigitoAgenciaConta = line.Substring(71, 1);
            string NomeEmpresa = line.Substring(72, 30);
            string NomeBanco = line.Substring(102, 30);
            string Filler6 = line.Substring(132, 10);
            string CodigoRetorno = line.Substring(142, 1);
            string DataGeracao = line.Substring(143, 8);
            string HoraGeracao = line.Substring(151, 6);
            string NrArquivo = line.Substring(157, 6);
            string VersaoLayout = line.Substring(163, 3);
            string Filler7 = line.Substring(166, 5);
            string ExclusivoBanco = line.Substring(171, 20);
            string Filler9 = line.Substring(191, 49);

            var Header = new DTOCnab.SafraHeader();

            Header.cnpj = Convert.ToInt64(NrInscricao);
            
            Header.dataArquivo = DateTime.ParseExact(DataGeracao, "ddMMyyyy", null);

            Header.empresa = NomeEmpresa.TrimEnd(' ');


            Header.ContaAnalisada.Add(new DTOCnab.ContaAnalisada()
            {
                Agencia = NrAgencia,
                Conta = CCEmpresa
            });

        
        



        }

        public static void ItauHeaderLoteCCorrente(string CNABFile)
        {
            string CodigoBanco = CNABFile.Substring(0, 3);
            string CodigoLote = CNABFile.Substring(3, 4);
            string RegistroHeader = CNABFile.Substring(7, 1);
            string TpOperacao = CNABFile.Substring(8, 1);
            string TpServico = CNABFile.Substring(9, 2);
            string FormaLancamento = CNABFile.Substring(11, 2);
            string NrVersaoLayout = CNABFile.Substring(13, 3);
            string Filler1 = CNABFile.Substring(16, 1);
            string TpCliente = CNABFile.Substring(17, 1);
            string NrCliente = CNABFile.Substring(18, 14);
            string IdTpConta = CNABFile.Substring(32, 4);
            string Filler2 = CNABFile.Substring(36, 11);
            string CdEmpresaBanco = CNABFile.Substring(47, 5);
            string Filler3 = CNABFile.Substring(52, 1);
            string NrAgenciaConta = CNABFile.Substring(53, 4);
            string DgAgencia = CNABFile.Substring(57, 1);
            string Filler4 = CNABFile.Substring(58, 7);
            string CCCliente = CNABFile.Substring(65, 5);
            string Filler5 = CNABFile.Substring(70, 1);
            string DigitoConta = CNABFile.Substring(71, 1);
            string NomeEmpresa = CNABFile.Substring(72, 30);
            string Filler6 = CNABFile.Substring(102, 40);
            string DataSaldoInicial = CNABFile.Substring(142, 8);
            string ValorSaldoInicial = CNABFile.Substring(150, 18);
            string SituacaoSaldoInicial = CNABFile.Substring(168, 1);
            string PosicaoSaldoInicial = CNABFile.Substring(169, 1);
            string MoedaExtrato = CNABFile.Substring(170, 3);
            string NrSequenciaExtrato = CNABFile.Substring(173, 5);
            string Filller7 = CNABFile.Substring(178, 62);

        }

        public static void ItauDetalheECCorrente(string CNABFile) // IMPORTANTE
        {



            string CodBanco = CNABFile.Substring(0,3);                    
            string CodLote = CNABFile.Substring(3,4);                     
            string TpRegistro = CNABFile.Substring(7,1);                  
            string NrRegistro = CNABFile.Substring(8,5);                  
            string Segmento = CNABFile.Substring(13,1);                   
            string Lancamento = CNABFile.Substring(14,1);                 
            string Filler1 = CNABFile.Substring(15,2);                    
            string TpInscricao = CNABFile.Substring(17,1);                
            string NrInscricao = CNABFile.Substring(18,14);               
            string CdHistorico = CNABFile.Substring(32,6);                
            string Filler2 = CNABFile.Substring(38,9);                    
            string Convenio = CNABFile.Substring(47,5);                   
            string Filler3 = CNABFile.Substring(52,1);                    
            string Agencia = CNABFile.Substring(53,4);                    
            string DgAgencia = CNABFile.Substring(57,1);                  
            string Filler4 = CNABFile.Substring(58,7);                    
            string ContaCorrente = CNABFile.Substring(65,5);              
            string Filler5 = CNABFile.Substring(70,1);                    
            string DgAgenciaConta = CNABFile.Substring(71,1);             
            string NomeEmpresa = CNABFile.Substring(72,30);               
            string UsoBanco = CNABFile.Substring(102,6);                  
            string Natureza = CNABFile.Substring(108,3);                  
            string TpComplemento = CNABFile.Substring(111,2);             
            string BancoOrigem = CNABFile.Substring(113,3);               
            string AgenciaOrigem = CNABFile.Substring(116,5);             
            string AgCtaOrigem = CNABFile.Substring(121,12);              
            string CPMF = CNABFile.Substring(133,1);                      
            string DataContabil = CNABFile.Substring(134,8);              
            string DataLancamento = CNABFile.Substring(142,9);            
            string Valor = CNABFile.Substring(150,18);                    
            string TpLancamento = CNABFile.Substring(168,1);              
            string CategoriaLancamento = CNABFile.Substring(169,3);
            string CodLancamento = CNABFile.Substring(172,4);       
            string Historico = CNABFile.Substring(176,25);          
            string AgenciadeOrigem = CNABFile.Substring(201,4);                   
            string Complemento = CNABFile.Substring(205,2);     
            string ContadeOrigem = CNABFile.Substring(207,5);      
            string DacAgCtadeOrigem = CNABFile.Substring(202,1);       
            string TipoInscricaoEmitente = CNABFile.Substring(213,1);                   
            string NrInscricaoEmitente = CNABFile.Substring(214,14);              
            string Filler6 = CNABFile.Substring(228, 6);              
            string NrDocumento = CNABFile.Substring(234, 6);              


        }
        
        public static void ItauTrailerLote(string CNABFile)
        {
           
            string CdBanco = CNABFile.Substring(0,3);
            string CdLote = CNABFile.Substring(3,4);
            string TpRegistro = CNABFile.Substring(7,1);
            string Filler1 = CNABFile.Substring(8,9);
            string TpInscricao = CNABFile.Substring(17,1);
            string NrInscricao = CNABFile.Substring(18,14);
            string Filler2 = CNABFile.Substring(32,15);
            string Convenio = CNABFile.Substring(47, 5);
            string Filler3 = CNABFile.Substring(52, 1);
            string Agencia = CNABFile.Substring(53,4);
            string DigitoAgencia = CNABFile.Substring(57,1);
            string Filler4 = CNABFile.Substring(58,7);
            string CCorrenteEmpresa = CNABFile.Substring(65,5);
            string Filler5 = CNABFile.Substring(70,1);
            string DigitoCCorrente = CNABFile.Substring(71,1);
            string Filler6 = CNABFile.Substring(72,16);
            string Filler7 = CNABFile.Substring(88,54);
            string DataSaldoInicial = CNABFile.Substring(142,8);
            string ValorSaldoInicial = CNABFile.Substring(150,18);
            string SituacaoSaldoFinal = CNABFile.Substring(168,1);
            string StatusSaldoFinal = CNABFile.Substring(169,1);
            string NrRegistrosLote = CNABFile.Substring(170,6);
            string SomaDebito = CNABFile.Substring(176,18);
            string SomaCredito = CNABFile.Substring(194,18);
            string TotalNaoContabil = CNABFile.Substring(212,18);
            string Filler8 = CNABFile.Substring(230,10);
            

        }

        public static void ItauHeaderLoteAplicacao(string CNABFile)
        {
            string CdBanco = CNABFile.Substring(0, 3);
            string CdLote = CNABFile.Substring(3, 4);
            string TpRegistro = CNABFile.Substring(7, 1);
            string TpOperacao = CNABFile.Substring(8, 1);
            string Servico = CNABFile.Substring(9, 2);
            string FormaLancamento = CNABFile.Substring(11, 2);
            string LayOut = CNABFile.Substring(13, 3);
            string Filler1 = CNABFile.Substring(16, 1);
            string TpInscricao = CNABFile.Substring(17, 1);
            string NrInscricao = CNABFile.Substring(18, 14);
            string TpConta = CNABFile.Substring(32, 4);
            string Filler2 = CNABFile.Substring(36, 11);
            string Convenio = CNABFile.Substring(47, 5);
            string Filler3 = CNABFile.Substring(52, 1);
            string Agencia = CNABFile.Substring(53, 4);
            string DACAgencia = CNABFile.Substring(57, 1);
            string Filler4 = CNABFile.Substring(58, 7);
            string Conta = CNABFile.Substring(65, 5);
            string Filler5 = CNABFile.Substring(70, 1);
            string DACAgCta = CNABFile.Substring(71, 1);
            string NomeEmpresa = CNABFile.Substring(72, 30);
            string Filler6 = CNABFile.Substring(102, 40);
            string DataInicial = CNABFile.Substring(142, 8);
            string ValorInicial = CNABFile.Substring(150, 18);
            string SituacaoInicial = CNABFile.Substring(168, 1);
            string StatusInicial = CNABFile.Substring(169, 1);
            string Moeda = CNABFile.Substring(170, 3);
            string SequenciaExtrato = CNABFile.Substring(173, 5);
            string Filler7 = CNABFile.Substring(178, 62);

        }

        public static void ItauDetalheEAplicacao(string CNABFile)
        {
            string CdBanco = CNABFile.Substring(0, 3);
            string CdLote = CNABFile.Substring(3, 4);
            string TpRegistro = CNABFile.Substring(7, 1);
            string NrRegistro = CNABFile.Substring(8, 5);
            string Segmento = CNABFile.Substring(13, 1);
            string IdLancamento = CNABFile.Substring(14, 1);
            string Filler1 = CNABFile.Substring(15, 2);
            string TpInscricao = CNABFile.Substring(17, 1);
            string NrInscricao = CNABFile.Substring(18, 14);
            string Filler2 = CNABFile.Substring(32, 15);
            string Convenio = CNABFile.Substring(47, 5);
            string Filler3 = CNABFile.Substring(52, 1);
            string Agencia = CNABFile.Substring(53, 4);
            string DACAgencia = CNABFile.Substring(57, 1);
            string Filler4 = CNABFile.Substring(58, 7);
            string Conta = CNABFile.Substring(65, 5);
            string Filler5 = CNABFile.Substring(70, 1);
            string DACAgCta = CNABFile.Substring(71, 1);
            string NomeEmpresa = CNABFile.Substring(72, 30);
            string UsoBanco1 = CNABFile.Substring(102, 6);
            string Natureza = CNABFile.Substring(108, 3);
            string UsoBanco2 = CNABFile.Substring(111, 23);
            string DataContabil = CNABFile.Substring(134, 8);
            string DataLancamento = CNABFile.Substring(142, 8);
            string Valor = CNABFile.Substring(150, 18);
            string TpLancamento = CNABFile.Substring(168, 1);
            string CatLancamento = CNABFile.Substring(169, 3);
            string CdLancamento = CNABFile.Substring(172, 4);
            string Historico = CNABFile.Substring(176, 25);
            string Filler6 = CNABFile.Substring(201, 39);

        }

        public static void ItauTrailerAplicacao(string CNABFile)
        {
            string CdBanco = CNABFile.Substring(0, 3);
            string CdLote = CNABFile.Substring(3, 4);
            string TpRegistro = CNABFile.Substring(7, 1);
            string Filler1 = CNABFile.Substring(8, 9);
            string TpInscricao = CNABFile.Substring(17, 1);
            string NrInscricao = CNABFile.Substring(18, 14);
            string Filler2 = CNABFile.Substring(32, 15);
            string Convenio = CNABFile.Substring(47, 5);
            string Filler3 = CNABFile.Substring(52, 1);
            string Agencia = CNABFile.Substring(53, 4);
            string DACAgencia = CNABFile.Substring(57, 1);
            string Filler4 = CNABFile.Substring(58, 7);
            string Conta = CNABFile.Substring(65, 5);
            string Filler5 = CNABFile.Substring(70, 1);
            string DACConta = CNABFile.Substring(71, 1);
            string Filler6 = CNABFile.Substring(72, 16);
            string Filler7 = CNABFile.Substring(88, 54);
            string DataFinal = CNABFile.Substring(142, 8);
            string Valorfinal = CNABFile.Substring(150, 18);
            string SituacaoFinal = CNABFile.Substring(168, 1);
            string Status = CNABFile.Substring(169, 1);
            string QtdeRegistros = CNABFile.Substring(170, 6);
            string TotalDebito = CNABFile.Substring(176, 18);
            string TotalCredito = CNABFile.Substring(194, 18);
            string TotalValoresNContabDebito = CNABFile.Substring(212, 14);
            string TotalValoresNContabCredito = CNABFile.Substring(226, 14);

            
        }

        public static void ItauTrailerArquivo(string CNABFile)
        {
            string CdBanco = CNABFile.Substring(0, 3);
            string CdLote = CNABFile.Substring(3, 4);
            string TpRegistro = CNABFile.Substring(7, 1);
            string Filler1 = CNABFile.Substring(8, 9);
            string TotalLotes = CNABFile.Substring(17, 6);
            string TotalRegistros = CNABFile.Substring(23, 6);
            string QuantidadeContasConciliacao = CNABFile.Substring(29, 6);
            string Filler2 = CNABFile.Substring(35, 205);

        }

        public static void BradescoHeaderArquivo(string CNABFile)
        {

            string CdBanco = CNABFile.Substring(0, 3);
            string LoteServico = CNABFile.Substring(3, 4);
            string HeaderArquivo = CNABFile.Substring(7, 1);
            string Filler1 = CNABFile.Substring(8, 9);
            string TpInscricaoEmpresa = CNABFile.Substring(17, 1);
            string NrInscricaoEmpresa = CNABFile.Substring(18, 14);
            string CdConvenio = CNABFile.Substring(32, 20);
            string AgConta = CNABFile.Substring(52, 5);
            string DACAgencia = CNABFile.Substring(57, 1);
            string ContaCorrente = CNABFile.Substring(58, 12);
            string DACConta = CNABFile.Substring(70, 1);
            string DACAgConta = CNABFile.Substring(71, 1);
            string NmEmpresa = CNABFile.Substring(72, 30);
            string NmBanco = CNABFile.Substring(102, 30);
            string Filler2 = CNABFile.Substring(132, 10);
            string CdRemessa = CNABFile.Substring(142, 1);
            string DtArquivo = CNABFile.Substring(143, 8);
            string HrArquivo = CNABFile.Substring(151, 6);
            string NrArquivo = CNABFile.Substring(157, 6);
            string NrVersao = CNABFile.Substring(163, 3);
            string DensidadeArquivo = CNABFile.Substring(166, 5);
            string UsoBanco = CNABFile.Substring(171, 20);
            string UsoEmpresa = CNABFile.Substring(191, 20);
            string Filler3 = CNABFile.Substring(211, 29);
           

        }

        public static void BradescoHeaderLote(string CNABFile)
        {
            
            string CdBanco = CNABFile.Substring(0, 3);
            string LtServico = CNABFile.Substring(3, 4);
            string RgHeaderLote = CNABFile.Substring(7, 1);
            string TpOperacao = CNABFile.Substring(8, 1);
            string TpServico = CNABFile.Substring(9, 2);
            string Lancamento = CNABFile.Substring(11, 2);
            string NrLayout = CNABFile.Substring(13, 3);
            string Filler1 = CNABFile.Substring(16, 1);
            string TpInscricao = CNABFile.Substring(17, 1);
            string NrInscricao = CNABFile.Substring(18, 14);
            string CdConvenio = CNABFile.Substring(32, 20);
            string AgConta = CNABFile.Substring(52, 5);
            string DACAgencia = CNABFile.Substring(57, 1);
            string NrContaCorrente = CNABFile.Substring(58, 12);
            string DACConta = CNABFile.Substring(70, 1);
            string DACAgConta = CNABFile.Substring(71, 1);
            string NmEmpresa = CNABFile.Substring(72, 30);
            string Filler2 = CNABFile.Substring(102, 40);
            string DataSaldo = CNABFile.Substring(142, 8);
            string ValorSaldo = CNABFile.Substring(150, 16);
            string TpSaldo = CNABFile.Substring(168, 1);
            string PosSaldo = CNABFile.Substring(169, 1);
            string Moeda = CNABFile.Substring(170, 3);
            string NrExtrato = CNABFile.Substring(173, 5);
            string Filler3 = CNABFile.Substring(178, 62);
            
        }

        public static void BradescoDetalheECCorrente(string CNABFile)
        {
            
            string CdCompensacao = CNABFile.Substring(0, 3);
            string LtServico = CNABFile.Substring(3, 4);
            string TpRegistro = CNABFile.Substring(7, 1);
            string NrSequencial = CNABFile.Substring(8, 5);
            string CdSegmento = CNABFile.Substring(13, 1);
            string Filler1 = CNABFile.Substring(14, 3);
            string TpInscricao = CNABFile.Substring(17, 1);
            string NrInscricao = CNABFile.Substring(18, 14);
            string CdConvenio = CNABFile.Substring(32, 20);
            string AgConta = CNABFile.Substring(52, 5);
            string DACAgencia = CNABFile.Substring(57, 1);
            string ContaCorrente = CNABFile.Substring(58, 12);
            string DACConta = CNABFile.Substring(70, 1);
            string DACAgConta = CNABFile.Substring(71, 1);
            string Empresa = CNABFile.Substring(72, 30);
            string Filler2 = CNABFile.Substring(102, 6);
            string NaturezaLancamento = CNABFile.Substring(108, 3);
            string TpLancamento = CNABFile.Substring(111, 2);
            string ComplementoLancamento = CNABFile.Substring(113, 20);
            string CMPF = CNABFile.Substring(133, 1);
            string DataContabil = CNABFile.Substring(134, 8);
            string DataLancamento = CNABFile.Substring(142, 8);
            string ValorLancamento = CNABFile.Substring(150, 18);
            string TpComplementoLancamento = CNABFile.Substring(168, 1);
            string CategoriaLancamento = CNABFile.Substring(169, 3);
            string CdBanco = CNABFile.Substring(172, 4);
            string DescricaoLancamento = CNABFile.Substring(176, 25);
            string NrDocumento = CNABFile.Substring(201, 39);


        }

        public static void BradescoTrailerLote(string CNABFile)
        {

            string CdCompensacao = CNABFile.Substring(0, 3);
            string LtServico = CNABFile.Substring(3, 4);
            string TpRegistro = CNABFile.Substring(7, 1);
            string Filler1 = CNABFile.Substring(8, 9);
            string TpInscricao = CNABFile.Substring(17, 1);
            string NrInscricao = CNABFile.Substring(18, 14);
            string CdConvenio = CNABFile.Substring(32, 20);
            string AgConta = CNABFile.Substring(52, 5);
            string DACAgencia = CNABFile.Substring(57, 1);
            string ContaCorrente = CNABFile.Substring(58, 12);
            string DACConta = CNABFile.Substring(70, 1);
            string DACAgConta = CNABFile.Substring(71, 1);
            string Filler2 = CNABFile.Substring(72, 16);
            string VinculadoDm1 = CNABFile.Substring(88, 18);
            string LimiteConta = CNABFile.Substring(106, 18);
            string VinculadoDAnterior = CNABFile.Substring(124, 18);
            string DataSaldoFinal = CNABFile.Substring(142, 8);
            string ValorSaldoFinal = CNABFile.Substring(150, 18);
            string SituacaoSaldoFinal = CNABFile.Substring(168, 1);
            string PosicaoSaldoFinal = CNABFile.Substring(169, 1);
            string NrRegistrosLote = CNABFile.Substring(170, 6);
            string TotalDebito = CNABFile.Substring(176, 18);
            string TotalCredito = CNABFile.Substring(194, 18);
            string Filler3 = CNABFile.Substring(212, 28);



        }
        
        public static void BradescoTrailerArquivo(string CNABFile)
        {

            string CdBanco = CNABFile.Substring(0, 3);
            string LtServico = CNABFile.Substring(3, 4);
            string TpRegistro = CNABFile.Substring(7, 1);
            string Filler1 = CNABFile.Substring(8, 9);
            string NrLotes = CNABFile.Substring(17, 6);
            string NrRegistros = CNABFile.Substring(23, 6);
            string NrContas = CNABFile.Substring(29, 6);
            string Filler2 = CNABFile.Substring(35, 205);

        }


    }
}
