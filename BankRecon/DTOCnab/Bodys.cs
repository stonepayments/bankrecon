﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOCnab
{
    public class ItauHeader
    {
        public string Valor { get; set; }

    }

    public class ItauBody
    {
  
    }

    public class ItauTrailer
    {

    }

    public class BradescoHeader
    {

    }

    public class BradescoBody
    {

    }

    public class BradescoTrailer
    {



    }

    public class BBrasilHeader
    {

    }

    public class BBrasilBody
    {

    }

    public class BBrasilTrailer
    {

    }

    public class SafraHeader
    {
        public string empresa { get; set; }
       
        public long cnpj { get; set; }

        public List<ContaAnalisada> ContaAnalisada { get; set; }

        public DateTime dataGeracao { get; set; }

        public DateTime dataArquivo { get; set; }

        public SafraHeader()
        {
            ContaAnalisada = new List<ContaAnalisada>();
        }

    }

    public class ContaAnalisada
    {
        public string Agencia { get; set; }

        public string Conta { get; set; }
    }


}
